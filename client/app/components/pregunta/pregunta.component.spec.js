'use strict';

describe('Component: pregunta', function() {
  // load the component's module
  beforeEach(module('acosoEnLineaApp.pregunta'));

  var preguntaComponent;

  // Initialize the component and a mock scope
  beforeEach(inject(function($componentController) {
    preguntaComponent = $componentController('pregunta', {});
  }));

  it('should ...', function() {
    expect(1).toEqual(1);
  });
});
