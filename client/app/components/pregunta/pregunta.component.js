'use strict';
const angular = require('angular');
const _ = require('lodash');
const $ = require('jquery');
export class preguntaComponent {
  constructor($scope, $state, $http, appConfig, $timeout, $window) {
    'ngInject';
    this.$scope = $scope;
    this.$state = $state;
    this.$http = $http;
    this.$timeout = $timeout;
    this.$window = $window;
    this.index = 0;
    this.preguntas = appConfig.preguntas;
    this.result = {
      cat1: 0,
      cat2: 0,
      cat3: 0
    };
    this.setClasses();
    this.$window.localStorage.setItem('respuestas', '');
  }

  setClasses() {
    let backgroundColors = ['success', 'info', 'warning', 'danger'];
    let lastBG;

    this.preguntas.forEach((pregunta, index) => {
      pregunta.classes = [];
      pregunta.classes.push(`pregunta-${pregunta.idPregunta}`);
      // par / impar
      if(index % 2 == 0) {
        pregunta.classes.push('par');
      }
      else {
        pregunta.classes.push('impar');
      }
      // asignar color diferente al anterior
      pregunta.classes.push( getColor() );
    });
    
    function getColor() {
      let currentBG = _.sample(backgroundColors);
      if(lastBG === currentBG) {
        return getColor();
      }
      lastBG = currentBG;
      return currentBG;
    }
  }

  goNext(respuesta) {
    if(this.animatingBtn) {
      return;
    }
    this.animateBtn(respuesta, () => {
      this.preguntas[this.index].respuesta = respuesta;
      if (this.index < (this.preguntas.length-1)) {
        this.index++;
      } else {
        this.calculoResultado();
        this.addEncuesta();
        this.$state.go('main.resultado', {general: false, result: this.result}, { reload: true });
      }
    });
  }

  animateBtn(respuesta, cb) {
    let iconsToShow = 2;
    let animationDuration = 1500;
    this.iconSelected = this.getOneIcon(respuesta);
    this.animatingBtn = true;
    let $btn;
    if(respuesta === 'SI') {
      $btn = $('.btn-si');
    }
    else if(respuesta === 'NO') {
      $btn = $('.btn-no');
    }
    else if(respuesta === 'NOSE') {
      $btn = $('.btn-nose')
    }
    // shake
    $btn.addClass('shake');
    this.$timeout(() => {
      $btn.removeClass('shake');
      this.animatingBtn = false;
      cb();
    }, animationDuration);
  }

  getOneIcon(respuesta) {
    let newIcon;
    if(respuesta === 'SI') {
      newIcon = '#' + _.sample(['001-llanto', '002-triste', '004-enojado-1', '006-vomito', '009-enojado-3', '014-emoji-1']);
    }
    else if (respuesta === 'NO') {
      newIcon = '#' + _.sample(['007-enojado-2', '010-conmocionado', '012-conmocionado-1', '013-emoji', '002-triste']);
    }
    else if (respuesta === 'NOSE') {
      return '#012-conmocionado-1';
    }
    if(newIcon === this.iconSelected) {
      return this.getOneIcon(respuesta);
    }
    return newIcon;
  }

  calculoResultado() {
    var cat1 = 0;
    var cat2 = 0;
    var cat3 = 0;
    var catTotal1 = 0;
    var catTotal2 = 0;
    var catTotal3 = 0;
    for (var i = 0; i < this.preguntas.length; i++) {

      //Acoso
      if (i == 0 || i == 1 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8) {
        if (this.preguntas[i].respuesta == 'SI') {
          cat1++;
        }
        if (this.preguntas[i].respuesta == 'SI' || this.preguntas[i].respuesta == 'NO') {
          catTotal1++;
        }
      }
      //Discurso de odio
      if (i == 2 || i == 7) {
        if (this.preguntas[i].respuesta == 'SI') {
          cat2++;
        }
        if (this.preguntas[i].respuesta == 'SI' || this.preguntas[i].respuesta == 'NO') {
          catTotal2++;
        }
      }
      //Vigilancia
      if (i == 3 || i == 4 || i == 5) {
        if (this.preguntas[i].respuesta == 'SI') {
          cat3++;
        }
        if (this.preguntas[i].respuesta == 'SI' || this.preguntas[i].respuesta == 'NO') {
          catTotal3++;
        }
      }
    }
    this.result.cat1 = ((catTotal1 == 0) ? 0 : (100 / catTotal1)) * cat1;
    this.result.cat2 = ((catTotal2 == 0) ? 0 : (100 / catTotal2)) * cat2;
    this.result.cat3 = ((catTotal3 == 0) ? 0 : (100 / catTotal3)) * cat3;
  }

  addEncuesta() {
    this.$http.post('/api/encuestas', {
      preguntas: this.preguntas,
      result: this.result
    });
    this.$window.localStorage.setItem('respuestas', JSON.stringify(this.preguntas));
  }

  $onInit() {}
}

export default angular.module('acosoEnLineaApp.pregunta', [])
  .component('pregunta', {
    template: require('./pregunta.html'),
    controller: preguntaComponent,
    controllerAs: 'vm',
  })
  .name;
