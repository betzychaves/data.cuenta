'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './resultado.events';

var ResultadoSchema = new mongoose.Schema({
  cat1: Number,
  cat2: Number,
  cat3: Number
});

registerEvents(ResultadoSchema);
export default mongoose.model('Resultado', ResultadoSchema);
