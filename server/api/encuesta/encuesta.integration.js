'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newEncuesta;

describe('Encuesta API:', function() {
  describe('GET /api/encuestas', function() {
    var encuestas;

    beforeEach(function(done) {
      request(app)
        .get('/api/encuestas')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          encuestas = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      encuestas.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/encuestas', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/encuestas')
        .send({
          name: 'New Encuesta',
          info: 'This is the brand new encuesta!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newEncuesta = res.body;
          done();
        });
    });

    it('should respond with the newly created encuesta', function() {
      newEncuesta.name.should.equal('New Encuesta');
      newEncuesta.info.should.equal('This is the brand new encuesta!!!');
    });
  });

  describe('GET /api/encuestas/:id', function() {
    var encuesta;

    beforeEach(function(done) {
      request(app)
        .get(`/api/encuestas/${newEncuesta._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          encuesta = res.body;
          done();
        });
    });

    afterEach(function() {
      encuesta = {};
    });

    it('should respond with the requested encuesta', function() {
      encuesta.name.should.equal('New Encuesta');
      encuesta.info.should.equal('This is the brand new encuesta!!!');
    });
  });

  describe('PUT /api/encuestas/:id', function() {
    var updatedEncuesta;

    beforeEach(function(done) {
      request(app)
        .put(`/api/encuestas/${newEncuesta._id}`)
        .send({
          name: 'Updated Encuesta',
          info: 'This is the updated encuesta!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedEncuesta = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedEncuesta = {};
    });

    it('should respond with the updated encuesta', function() {
      updatedEncuesta.name.should.equal('Updated Encuesta');
      updatedEncuesta.info.should.equal('This is the updated encuesta!!!');
    });

    it('should respond with the updated encuesta on a subsequent GET', function(done) {
      request(app)
        .get(`/api/encuestas/${newEncuesta._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let encuesta = res.body;

          encuesta.name.should.equal('Updated Encuesta');
          encuesta.info.should.equal('This is the updated encuesta!!!');

          done();
        });
    });
  });

  describe('PATCH /api/encuestas/:id', function() {
    var patchedEncuesta;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/encuestas/${newEncuesta._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Encuesta' },
          { op: 'replace', path: '/info', value: 'This is the patched encuesta!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedEncuesta = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedEncuesta = {};
    });

    it('should respond with the patched encuesta', function() {
      patchedEncuesta.name.should.equal('Patched Encuesta');
      patchedEncuesta.info.should.equal('This is the patched encuesta!!!');
    });
  });

  describe('DELETE /api/encuestas/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/encuestas/${newEncuesta._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when encuesta does not exist', function(done) {
      request(app)
        .delete(`/api/encuestas/${newEncuesta._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
