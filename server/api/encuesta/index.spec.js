'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var encuestaCtrlStub = {
  index: 'encuestaCtrl.index',
  show: 'encuestaCtrl.show',
  create: 'encuestaCtrl.create',
  upsert: 'encuestaCtrl.upsert',
  patch: 'encuestaCtrl.patch',
  destroy: 'encuestaCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var encuestaIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './encuesta.controller': encuestaCtrlStub
});

describe('Encuesta API Router:', function() {
  it('should return an express router instance', function() {
    encuestaIndex.should.equal(routerStub);
  });

  describe('GET /api/encuestas', function() {
    it('should route to encuesta.controller.index', function() {
      routerStub.get
        .withArgs('/', 'encuestaCtrl.index')
        .should.have.been.calledOnce;
    });
  });

  describe('GET /api/encuestas/:id', function() {
    it('should route to encuesta.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'encuestaCtrl.show')
        .should.have.been.calledOnce;
    });
  });

  describe('POST /api/encuestas', function() {
    it('should route to encuesta.controller.create', function() {
      routerStub.post
        .withArgs('/', 'encuestaCtrl.create')
        .should.have.been.calledOnce;
    });
  });

  describe('PUT /api/encuestas/:id', function() {
    it('should route to encuesta.controller.upsert', function() {
      routerStub.put
        .withArgs('/:id', 'encuestaCtrl.upsert')
        .should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/encuestas/:id', function() {
    it('should route to encuesta.controller.patch', function() {
      routerStub.patch
        .withArgs('/:id', 'encuestaCtrl.patch')
        .should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/encuestas/:id', function() {
    it('should route to encuesta.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'encuestaCtrl.destroy')
        .should.have.been.calledOnce;
    });
  });
});
