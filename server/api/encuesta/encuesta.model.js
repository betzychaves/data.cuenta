'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './encuesta.events';

var EncuestaSchema = new mongoose.Schema({
  createDate: {
    type: Date,
    default: Date.now,
  },
  pais: String,
  testimonio: String,
  preguntas: [{
    idPregunta: Number,
    respuesta: String
  }],
  result: {
    cat1: Number,
    cat2: Number,
    cat3: Number,
  }
});

registerEvents(EncuestaSchema);
export default mongoose.model('Encuesta', EncuestaSchema);
