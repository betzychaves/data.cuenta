/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/encuestas              ->  index
 * POST    /api/encuestas              ->  create
 * GET     /api/encuestas/:id          ->  show
 * PUT     /api/encuestas/:id          ->  upsert
 * PATCH   /api/encuestas/:id          ->  patch
 * DELETE  /api/encuestas/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Encuesta from './encuesta.model';
import Resultado from './../resultado/resultado.model';
import BlueBird from 'bluebird';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function (entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch (err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Encuestas
export function index(req, res) {
  return Encuesta.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Encuesta from the DB
export function show(req, res) {
  return Encuesta.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Checks if the user is not a robot
export function captcha(req, res) {
  const token = req.body.token;
  callRecapGoogle(token, function (isARobot) {
    return res.json(isARobot);
  });
}

//Calls google recaptcha service
export function callRecapGoogle(token, callback) {
  const url = 'https://www.google.com/recaptcha/api/siteverify';
  const serverKey = '6LcbcKoUAAAAAJw685eyZSVNK6wNCdDft9UOjGQ5';
  const request = require('request');
  request.post({ url: url, qs: { 'response': token, 'secret': serverKey } },
    (error, res, body) => {
      if (error) {
        console.error(error)
        return
      }
      console.log(body);
      var score = JSON.parse(body).score;
      if (score > 0.5) {
        return callback(false);
      } else {
        return callback(true);
      }
    });
}

// Creates a new Encuesta in the DB
export function create(req, res) {
  return Encuesta.create(req.body)
    .then(respondWithResult(res, 201))
    .then(calcGeneralResult(req.body.result))
    .catch(handleError(res));
}

//Calculate general result
export function calcGeneralResult(result) {
  return BlueBird.join(Resultado.findOne({}).exec(),
    Encuesta.count({})).spread(function (entity, count) {
      if (entity) {
        entity.cat1 = (entity.cat1 + result.cat1) / (count + 1);
        entity.cat2 = (entity.cat2 + result.cat2) / (count + 1);
        entity.cat3 = (entity.cat3 + result.cat3) / (count + 1);
        return entity.save();
      }
      return Resultado.create(result);
    });
}

// Upserts the given Encuesta in the DB at the specified ID
export function upsert(req, res) {
  if (req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return Encuesta.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true }).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Encuesta in the DB
export function patch(req, res) {
  if (req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return Encuesta.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Encuesta from the DB
export function destroy(req, res) {
  return Encuesta.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
